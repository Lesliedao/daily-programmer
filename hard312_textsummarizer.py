
# coding: utf-8

# In[1]:

import math, re


# In[2]:

in_file = 'hard312_textsummarizer_input.txt'
# Limit size of summary
max_sentences = 4


# In[3]:

# Remove characters to avoid interference when determining terms
def remove_char(sentences):
    return ' '.join(list(map(lambda s: re.sub('[\(\),:;]|( - )', '', s[:-1]), sentences))).lower()


# In[4]:

# Return the "cleaned text" to extract terms from, and text split by sentence
def preprocess_input(file):
    with open(file, 'r') as f:
        # Remove characters to avoid having them in the text altogether (e.g. Wikipedia footnotes)
#         raw_text = re.sub('(\[\w*(\s\w)*\])', '', re.sub('\s+', ' ', f.read().strip()))
        raw_text = re.sub('(\[[(\w)|(\s)]*\])',
                          '',
                          re.sub('\s+',
                                 ' ',
                                 f.read().strip()
                                )
                         )
        
    # Determine end of sentences, a bit more reliable than split on '. '
    text = ''
    for w in raw_text.split(' '):
        if w[-1] not in '.!?':
            # Mid-sentence words
            text += w + ' '
        elif w.count('.') > 1:
            # Acronym check
            text += w + ' '
        else:
            # Mark end of sentence with \t
            text += w + '\t'
    
    return remove_char(text.split('\t')[:-1]), text.split('\t')[:-1]


# In[5]:

# Determine a sentence's average score depending on its terms' tfidf
def score_sentence(term_scores, s):
    score = 0
    for t in term_scores:
        if t[0] in s:
            score += t[1]
    return score / len(s.split(' '))


# In[6]:

# Summarize a text by using at most max_sentences
def summarize(text, sentences, max_sentences = 3):
    N = len(sentences)
    terms = list(set(text.split(' ')))
    
    # Term frequency/Inverse document frequency dict. Use +1 to avoid DivideByZero
    tfidf = {}
    for t in terms:
        tfidf[t] = {'tf': text.count(t),
                    'idf': math.log(N / (1 + sum([t in s.lower() for s in sentences])))}
    
    # Create list: (term, tfidf)
    term_scores = list(map(lambda t: (t[0], (t[1]['tf'] * t[1]['idf'])), tfidf.items()))
    
    # Create list: (sentence, average term score of sentence)
    ranked_sentences = (list(map(lambda s: score_sentence(term_scores, s.lower()), sentences)))
    sentence_scores = list(zip(sentences, ranked_sentences))
    # Get the top sentences, and keep them in the same order as the input
    top_sentences = [sentences[i] for i in sorted(sorted(range(len(sentence_scores)),
                                                         key = lambda x: sentence_scores[x][1])[::-1][:min(max_sentences, N)])]
    
    return ' '.join(top_sentences)


# In[7]:

text, sentences = preprocess_input(in_file)


# In[8]:

' '.join(sentences)


# In[9]:

print(summarize(text, sentences, max_sentences))


# In[ ]:



