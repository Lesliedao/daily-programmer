##
# Prime numbers
##

while True:
	rawlim = raw_input("Upper limit: ")
	try:
		lim = int(rawlim)
	except ValueError:
		continue
	else:
		if lim < 1:
			continue
		else:
			break

for i in range(1, lim + 1):
	prime = True
	for j in range(1, i):
		if j == 1 or j == i:
			pass
		elif i % j == 0:
			prime = False
			break
		else:
			pass
	if prime == True:
		print i
	else:
		pass