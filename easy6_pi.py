import math, random

n = int(input('> '))

inside = 0
for _ in range(n):
    x, y = (random.uniform(-1, 1), random.uniform(-1, 1))
    if (math.sqrt(x ** 2 + y ** 2) <= 1): inside += 1

est = 4 * inside / n
print(f'Pi: \t\t{math.pi:.20f}')
print(f'Estimation: \t{est:.20f}')
print(f'Error: \t\t{math.fabs(est - math.pi):.20f}')