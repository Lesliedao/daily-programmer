##
# Easy 266 - Basic Graph Statistics: Node Degrees
##

N = int(input("> Amount of nodes: "))
nodes = [0 for x in range(N)]
matrix = [[0 for x in range(N)] for y in range(N)]
while True:
    try:
        a, b = input("> ").split()
    except ValueError:
        break
    else:
        a, b = int(a), int(b)
        nodes[a - 1] += 1
        nodes[b - 1] += 1
        matrix[a - 1][b - 1] = 1
        matrix[b - 1][a - 1] = 1

for i in range(N):
    print("Node %d has a degree of %d" % (i + 1, nodes[i]))

print("")

for i in range(N):
    for j in range(N):
        print(matrix[i][j], end = " ")
    print("")
