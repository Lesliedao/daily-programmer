## Standard deviation
## Berekent de standaardafwijking van een lijst getallen
## op 4 decimalen nauwkeurig.

import math

inputlist = raw_input("Number list: ")
list = [int(x) for x in inputlist.split()]
mean = sum(list) / len(list)
print "Mean:", mean
sqdev = float(sum([(x - mean)**2 for x in list]))
stddev = round(math.sqrt(sqdev / len(list)), 4)
print "Standard deviation:", stddev