##
# Texas Hold em
#
# Deals out cards for Texas Hold em
##

from random import choice

suits = ["Spades", "Clubs", "Hearts", "Diamonds"]
values = [2, 3, 4, 5, 6, 7, 8, 9, 10, "Jack", "Queen", "King", "Ace"]
cardsinplay = []

def pickacard(n):
	for i in range(n):
		while True:
			suit = choice(suits)
			value = choice(values)
			if (suit, value) in cardsinplay:
				continue
			else:
				break
		
		card = (suit, value)
		cardsinplay.append(card)
		if i == (n - 1):
			print value, "of", suit
		else:
			print value, "of", suit+",",

def burnacard():
	while True:
		suit = choice(suits)
		value = choice(values)
		if (suit, value) in cardsinplay:
			continue
		else:
			break
	
	card = (suit, value)
	cardsinplay.append(card)

while True:
	rawplayers = raw_input("How many players (2-8)? ")
	try:
		players = int(rawplayers)
	except ValueError:
		continue
	else:
		if players < 2 or players > 8:
			continue
		else:
			break

print ""
for i in range(players):
	if i == 0:
		print "Your hand:",
		pickacard(2)
	else:
		print "CPU", i, "hand:",
		pickacard(2)
		
print ""
burnacard()
print "Flop:",
pickacard(3)

burnacard()
print "Turn:",
pickacard(1)

burnacard()
print "River:",
pickacard(1)