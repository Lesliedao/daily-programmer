## Acronym expander
## Leest een string in en print dezelfde string met de afkortingen ingevuld.

message = raw_input("Enter message: ")
list = message.split()
acros = ['lol', 'dw', 'hf', 'gg', 'brb', 'g2g', 'wtf', 'wp', 'gl', 'imo']
expnd = ["laugh out loud", "don't worry",  "have fun", "good game", "be right back", "got to go",
	"what the fuck", "well played", "good luck", "in my opinion"]
dict = dict(zip(acros, expnd))
output = []
for word in list:
	if word.strip(".,:") in dict:
		output.append(dict[word.strip(".,:")])
	else:
		output.append(word)

print " ".join(output)