import random
import time
from math import floor

class GenericString(object):
    def __init__(self, goal, size):
        self.ALL_CHARS = [chr(i) for i in range(ord(' '), ord('~') + 1)]
        self.MOTHER_PROB = 0.5
        self.MUTATION_PROB = 0.70
        self.SURVIVAL_FRAC = 0.25
        self.goal = goal
        self.size = size
        self.pop = [self.generateIndividual(len(self.goal)) for _ in range(self.size)]

    def fitness(self, individual):
        return sum([individual[i] != self.goal[i] for i in range(len(self.goal))])

    def advanceGeneration(self):
        newPop = sorted(self.pop, key = self.fitness)[:floor(self.size * self.SURVIVAL_FRAC)]
        self.pop = newPop
        while len(newPop) < self.size:
            newPop.append(self.recombine(*random.sample(self.pop, 2)))
        self.pop = newPop

    def generateIndividual(self, length):
        return ''.join(random.choice(self.ALL_CHARS) for l in range(length))

    def recombine(self, mother, father):
        child = []
        for i in range(len(mother)):
            child.append(mother[i] if random.random() < self.MOTHER_PROB else father[i])
        if random.random() < self.MUTATION_PROB:
            child[random.randint(0, len(mother) - 1)] = random.choice(self.ALL_CHARS)
        return ''.join(child)

    def run(self):
        gen = 0
        oldMatch = ''
        t = time.time()
        while self.goal not in self.pop:
            bestMatch = min(self.pop, key = self.fitness)
            if oldMatch != bestMatch:
                print(f'Gen: {gen}\t| Fitness: {self.fitness(bestMatch)} \t| {bestMatch}')
                oldMatch = bestMatch
            self.advanceGeneration()
            gen += 1
        bestMatch = min(self.pop, key = self.fitness)
        print(f'Gen: {gen}\t| Fitness: {self.fitness(bestMatch)} \t| {bestMatch}')
        print(f'Target string {self.goal} found in the population in generation {gen}')
        print(f'Elapsed time is {(time.time() - t):.10} seconds')

g = GenericString('Hello, World!', 1000)
g.run()
