##
# Culling numbers
##

numbers = []

prompt = raw_input("> ").split(" ")

for number in prompt:
    if number not in numbers:
        numbers.append(number)

print ""
print " ".join(numbers)
