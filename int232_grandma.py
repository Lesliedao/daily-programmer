##
# Where should grandma's house go?
##

import timeit
from math import sqrt

timerstart = timeit.default_timer()

locations = []

with open("int232_grandma_input.txt") as f:
    for line in f:
        # map(function, iterable)
        locations.append(map(lambda x: float(x), line.strip().strip("(").strip(")").split(", ")))

n = len(locations)

# Lege matrix
distance = [[0 for x in range(n)] for x in range(n)]

def d(loc1, loc2):
    return sqrt(((loc1[0] - loc2[0]) ** 2) + ((loc1[1] - loc2[1]) ** 2))

for i in range(n):
    for j in range(n):
        distance[i][j] = d(locations[i], locations[j])

mindistances = []
for i in range(n):
    mindistances.append(min(x for x in distance[i] if x > 0))

indices = [locations[i] for i, j in enumerate(mindistances) if j == min(mindistances)]
print "(" + str(indices[0][0]) + ", " + str(indices[0][1]) + "), (" + str(indices[1][0]) + ", " + str(indices[1][1]) + ")"

timerstop = timeit.default_timer()

print "Runtime: %.5f seconds" % (timerstop - timerstart)
