##
# Easy 276 Rektangles
##

word, width, height = input("Input: word [SPACE] width [SPACE] height > ").split()

width, height = int(width), int(height)

word = word.upper()
wordLength = len(word)

for y in range(1 + (wordLength - 1) * height):
    for x in range(1 + (wordLength - 1) * width):
        # print("X", end = " ")
        # Check of de rij letters in elke kolom heeft
        if y % (wordLength - 1) == 0:
            # Letters in elke kolom
            # Check of het woord normaal of andersom gespeld moet worden adhv de rij en kolom
            if (y // (wordLength - 1)) % 2 == 0:
                if (x // (wordLength - 1)) % 2 == 0:
                    print(word[x % (wordLength - 1)], end = " ")
                else:
                    print(word[wordLength - 1 - x % (wordLength - 1)], end = " ")
            else:
                if (x // (wordLength - 1)) % 2 == 0:
                    print(word[wordLength - 1 - x % (wordLength - 1)], end = " ")
                else:
                    print(word[x % (wordLength - 1)], end = " ")
        else:
            # Alleen letters in kolommen waar x % (wordLength - 1) == 0
            if x % (wordLength - 1) == 0:
                if (y // (wordLength - 1)) % 2 == 0:
                    # De rij is even
                    if (x // (wordLength - 1)) % 2 == 0:
                        # Spel normaal
                        print(word[y % (wordLength - 1)], end = " ")
                    else:
                        # Spel andersom
                        print(word[wordLength - 1 - y % (wordLength - 1)], end = " ")
                else:
                    # De rij is oneven
                    if (x // (wordLength - 1)) % 2 == 0:
                        # Spel andersom
                        print(word[wordLength - 1 - y % (wordLength - 1)], end = " ")
                    else:
                        # Spel normaal
                        print(word[y % (wordLength - 1)], end = " ")
            else:
                print(" ", end = " ")
    print("")

