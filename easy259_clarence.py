##
# Clarence the slow typist
##

from math import sqrt

# Getallen hebben dezelfde index, . heeft index 10
distance = [[0 for x in range(11)] for x in range(11)]

for i in range(1, 10):
    x1 = (i - 1) % 3
    if i < 4:
        y1 = 0
    elif i < 7:
        y1 = 1
    else:
        y1 = 2
    # Afstand naar getallen 1-9
    for j in range(1, 10):
        x2 = (j - 1) % 3
        if j < 4:
            y2 = 0
        elif j < 7:
            y2 = 1
        else:
            y2 = 2

        distance[i][j] = sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
        distance[j][i] = sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

    # Afstand naar 0
    distance[i][0] = sqrt((1 - x1) ** 2 + (3 - y1) ** 2)
    distance[0][i] = sqrt((1 - x1) ** 2 + (3 - y1) ** 2)

    # Afstand naar .
    distance[i][10] = sqrt((0 - x1) ** 2 + (3 - y1) ** 2)
    distance[10][i] = sqrt((0 - x1) ** 2 + (3 - y1) ** 2)

distance[0][10] = 1
distance[10][0] = 1

ip = raw_input("> ")
total = 0

firstrun = True
for c in ip:
    try:
        index = int(c)
    except ValueError:
        index = 10

    if firstrun:
        oldindex = index
        firstrun = False

    total += distance[oldindex][index]

    oldindex = index

print "Distance: %.2f cm" % (total)
