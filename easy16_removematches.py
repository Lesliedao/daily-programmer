##
# Remove matches
# Remove characters from the first string that appear in the second string
##

string1 = raw_input("String to filter: ")
string2 = raw_input("Filtered letters: ")

print ''.join([x for x in string1 if x not in string2])
