##
# Easy 273 Getting a degree
##

from math import pi as PI

def radToDeg(radians):
    return radians * 180 / PI

def degToRad(degrees):
    return degrees * PI / 180

def fahrenheitToCelsius(F):
    return (F - 32) * 5 / 9

def fahrenheitToKelvin(F):
    return (F + 459.67) * 5 / 9

def celsiusToFahrenheit(C):
    return C * 9 / 5 + 32

def celsiusToKelvin(C):
    return C + 273.15

def kelvinToFahrenheit(K):
    return K * 9 / 5 - 459.67

def kelvinToCelsius(K):
    return K - 273.15

prompt = raw_input("> ")
value, conversion = float(prompt[:-2]), prompt[-2:]

if conversion == "dr":
    print "%.5fr" % degToRad(value)
elif conversion == "rd":
    print "%.5fd" % radToDeg(value)
elif conversion == "fc":
    print "%.5fc" % fahrenheitToCelsius(value)
elif conversion == "fk":
    print "%.5fk" % fahrenheitToKelvin(value)
elif conversion == "cf":
    print "%.5f" % celsiusToFahrenheit(value)
elif conversion == "ck":
    print "%.5fk" % celsiusToKelvin(value)
elif conversion == "kf":
    print "%.5ff" % kelvinToFahrenheit(value)
elif conversion == "kc":
    print "%.5fc" % kelvinToCelsius(value)
else:
    print "Invalid input"
