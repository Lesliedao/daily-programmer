var Todo = function() {
    var list = [];

    function addItem(item) {
        list.push(item);
    }

    function deleteItem(item) {
        var i = list.indexOf(item);
        if (i === -1) {
            console.log(item + " not found");
        }
        else {
            list.splice(i, 1);
        }
    }

    function viewList() {
        list.forEach(function(item) {console.log(item);});
    }

    return {
        add: addItem,
        del: deleteItem,
        view: viewList
    };
};

var myList = new Todo();

myList.add("Eggs");
myList.add("Milk");
myList.add("Coffee");
myList.view();
myList.del("Milk");
myList.del("Butter");
myList.view();
