import itertools

cards = []
while True:
    card = input().upper()
    if len(card) == 4:
        cards.append(card)
    else:
        break

# print(cards)
cardCombinations = itertools.combinations(cards, 3)
sets = []
for c in cardCombinations:
    status = True
    for i in range(4):
        uniques = len(set([x[i] for x in c]))
        if uniques != 3 and uniques != 1:
            status = False
    if status:
        sets.append(c)

for s in sets:
    print(' '.join(s))
