##
# Tree generation
##

from random import random

width, base, leaf = raw_input("> ").split()
width = int(width)
height = (width + 1) / 2

for i in range(height):
	leaves = []
	print " "*(height - 1 - i),
	for j in range (1 + 2 * i):
		if random() > 0.75:
			leaves.append("o")
		else:
			leaves.append(leaf)
	print "".join(leaves)
	

print " "*(height - 2), base*3