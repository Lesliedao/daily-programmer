rawTakuzu = '''222222121220
222202122212
221202222020
002222221222
222200222022
220202221222
222222002020
200221222222
220222212222
002222222022
222222200222
002220222222'''

inv = {0: 1, 1: 0}

class Takuzu(object):
    def __init__(self, raw):
        self.grid = list(map(lambda x: [int(n) for n in x], raw.split('\n')))
        self.size = len(self.grid)

    def printGrid(self):
        print('X', end = '\t')
        for i in range(self.size): print(i + 1, end = '\t')
        print()
        for j in range(self.size):
            print(j + 1, end = '\t')
            for n in self.grid[j]:
                print(n, end = '\t') if n != 2 else print(' ', end = '\t')
            print()
        print()

    # Elementwise comparison of rows and columns, returns a list with matches as booleans
    def compareRows(self, first, second):
        return [self.grid[first][i] == self.grid[second][i] for i in range(self.size)]

    def compareColumns(self, first, second):
        return [self.grid[i][first] == self.grid[i][second] for i in range(self.size)]

    def solve(self):
        counter = 1
        while counter > 0:
            counter = 0
            # TODO: If stuck, try and fill in values such that there are no duplicate rows/columns. Find rows/columns which still need two values to be plugged in and if so, try and find filled in rows/columns which are similar and swap the position of the empty fields
            # Perform checks: XX => YXXY, X.X => XYX, and same amounts check
            # Per row
            for r in range(self.size):
                # XX check
                for i in range(self.size - 1):
                    if self.grid[r][i] == self.grid[r][i + 1] and self.grid[r][i] != 2:
                        if i != 0:
                            if self.grid[r][i - 1] == 2:
                                self.grid[r][i - 1] = inv[self.grid[r][i]]
                                counter += 1
                        if i != (self.size - 2):
                            if self.grid[r][i + 2] == 2:
                                self.grid[r][i + 2] = inv[self.grid[r][i]]
                                counter += 1
                # X.X check
                for i in range(self.size - 2):
                    if self.grid[r][i] == self.grid[r][i + 2] and self.grid[r][i] != 2 and self.grid[r][i + 1] == 2:
                        self.grid[r][i + 1] = inv[self.grid[r][i]]
                        counter += 1
                # Same amounts check
                if self.grid[r].count(2) == abs(self.grid[r].count(0) - self.grid[r].count(1)) and self.grid[r].count(2) != 0:
                    self.grid[r] = [int(self.grid[r].count(0) > self.grid[r].count(1)) if n == 2 else n for n in self.grid[r]]
                    counter += 1
            # Per column
            for c in range(self.size):
                ## XX check
                for i in range(self.size - 1):
                    if self.grid[i][c] == self.grid[i + 1][c] and self.grid[i][c] != 2:
                        if i != 0:
                            if self.grid[i - 1][c] == 2:
                                self.grid[i - 1][c] = inv[self.grid[i][c]]
                                counter += 1
                        if i != (self.size - 2):
                            if self.grid[i + 2][c] == 2:
                                self.grid[i + 2][c] = inv[self.grid[i][c]]
                                counter += 1
                # X.X check
                for i in range(self.size - 2):
                    if self.grid[i][c] == self.grid[i + 2][c] and self.grid[i][c] != 2 and self.grid[i + 1][c] == 2:
                        self.grid[i + 1][c] = inv[self.grid[i][c]]
                        counter += 1
                # Same amounts check
                columnValues = [r[c] for r in self.grid]
                if columnValues.count(2) == abs(columnValues.count(0) - columnValues.count(1)) and columnValues.count(2) != 0:
                    for i in range(self.size):
                        if self.grid[i][c] == 2:
                            self.grid[i][c] = int(columnValues.count(0) > columnValues.count(1))
                    counter += 1

t = Takuzu(rawTakuzu)
print('Starting grid')
t.printGrid()
t.solve()
print('Output grid')
t.printGrid()
