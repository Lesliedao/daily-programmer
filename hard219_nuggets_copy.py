# pylint: disable=C0103,C0111,C0303,C0301
##
# The cave of prosperity (0/1 Knapsack)
##

from itertools import chain, combinations

with open("hard219_nuggets_input3.txt", "r") as f:
    linecount = 0
    for line in f:
        if linecount == 0:
            capacity = int(line.strip().replace(".", ""))
            linecount += 1
        elif linecount == 1:
            nNuggets = int(line.strip())
            break
    nuggets = []
    for line in f:
        nuggets.append(int(line[2:].strip()))

best = [0, 0]
for i in range((len(nuggets) + 1) / 2, len(nuggets) + 1):
    for combination in combinations(nuggets, i):
        if sum(combination) <= capacity and sum(combination) > sum(best):
            best = combination
print sum(best) / float(10000000)
for nug in best:
    print nug / float(10000000)
