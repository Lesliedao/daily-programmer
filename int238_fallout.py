import random
from sys import exit

def strcmp(s1, s2):
    if len(s1) != len(s2):
        return 0
    count = 0
    for i in range(len(s1)):
        count += s1[i] == s2[i]
    return count

EXITFLAG = {
    1: 'Incorrect input'
}

DIFFICULTY = int(input('Difficulty (1-5)? '))
if DIFFICULTY < 1 or DIFFICULTY > 5:
    print(EXITFLAG[1])
    exit(1)
NCHOICES = random.randint(3 + 2 * DIFFICULTY, 5 + 2 * DIFFICULTY)
WORDLENGTH = random.randint(2 + 2 * DIFFICULTY, 5 + 2 * DIFFICULTY)
print('Starting with difficulty %d' % DIFFICULTY)

with open('enable1.txt', 'r') as f:
    dictionary = f.read().replace('\n', ' ').split(' ')
    dictionary = list(map(lambda y: y.upper(), list(filter(lambda x: len(x) == WORDLENGTH, dictionary))))

WORDCHOICES = random.sample(dictionary, NCHOICES)
CORRECTWORD = random.choice(WORDCHOICES)
for w in WORDCHOICES:
    print(w)
ntries = 4
guessed = False

while ntries > 0 and not guessed:
    guess = input('Guess (%d left)? ' % ntries).upper()
    if guess in WORDCHOICES:
        ncorrect = strcmp(guess, CORRECTWORD)
        print('%d/%d correct' % (ncorrect, WORDLENGTH))
        if ncorrect == WORDLENGTH:
            guessed = True
    else:
        print('Input not in word list')
    ntries -= 1

if guessed:
    print('You win!')
else:
    print('You lost! The correct word was %s' % CORRECTWORD)
