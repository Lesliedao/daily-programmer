# pylint: disable = C0103
# C0103: invalid variable name

##
# Generators
##

import random

def lottery():
    for i in xrange(6):
        yield random.randint(1, 45)

    yield random.randint(1, 15)

for number in lottery():
    print "And the next number is.. %d!" % number

print ""

def count_to_ten():
    i = 1
    while i <= 10:
        yield i
        i += 1

for number in count_to_ten():
    print number

print ""

def fibonacci():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b

fibgen = fibonacci()

count = 0
while count < 10:
    count += 1
    print next(fibgen)
