with open('declaration.txt', 'r') as dfile:
    allLetters = [x[0] for x in dfile.read().split(' ')]

with open('bealecipher.txt', 'r') as cfile:
    allKeys = [int(x) - 1 for x in cfile.read().split(', ')]

print(''.join(allLetters[x] for x in allKeys))
