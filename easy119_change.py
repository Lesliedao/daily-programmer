## Change calculator
## Berekent het minimum aantal kwartjes, dubbeltjes, stuivers en centen
## dat nodig is om het wisselgeld te betalen. (rondt af op centen)

while True:
    rawchange = raw_input("How much change is owed? ")
    try:
        change = float(rawchange)
    except ValueError:
        continue
    else:
        if change < 0:
            continue
        else:
            break

rem = int(round(change, 2) * 100)

quarters = (rem - (rem % 25)) / 25
rem = rem - 25 * quarters

dimes = (rem - (rem % 10)) / 10
rem = rem - 10 * dimes

nickels = (rem - (rem % 5)) / 5
rem = rem - 5 * nickels

print "Quarters:", quarters
print "Dimes:", dimes
print "Nickels:", nickels
print "Cents:", rem
