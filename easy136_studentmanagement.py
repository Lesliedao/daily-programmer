##
# Student management
##

rawinp = raw_input("#students #grades: ")
inp = rawinp.split()
N = int(inp[0])
M = int(inp[1])

students = []
averages = []

for i in range(N):
	rawinp = raw_input("> ")
	inp = rawinp.split()
	students.append(inp[0])
	
	grades = []
	for j in range(1, len(inp)):
		grades.append(int(inp[j]))
	
	avg = round(sum(grades) / float(len(grades)), 2)
	averages.append(avg)
		
print round(sum(averages) / float(len(averages)), 2)
for i in range(N):
	print students[i], averages[i]