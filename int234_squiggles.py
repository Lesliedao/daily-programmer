with open('enable1.txt', 'r') as f:
    correctWords = f.read().split('\n')

    wordsToCheck = []
    while True:
        inWord = input('> ')
        if len(inWord) < 1:
            break
        else:
            wordsToCheck.append(inWord)
    
    for word in wordsToCheck:
        candidates = correctWords[:]
        for i in range(len(word)):
            correctlySpelled = True
            candidates = list(filter(lambda x: x.startswith(word[:i + 1]), candidates))
            if len(candidates) == 0:
                correctlySpelled = False
                print(word[:(i + 1)] + '<' + word[(i + 1):])
                break
        if correctlySpelled:
            print(word)

