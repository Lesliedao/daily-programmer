##
# Date Dilemma
#
# Dates starting with 4 digits use Y M D, others use M D Y
##

with open("easy245_datedilemma_input.txt", "r") as f:
    for line in f:
        datelist = line.replace("/", " ").replace("-", " ").strip().split(" ")

        # M D Y notatie omzetten naar Y M D notatie
        if len(datelist[0]) < 4:
            datelist = [datelist[2], datelist[0], datelist[1]]

        # Element 0: jaar, element 1: maand, element 2: dag
        # Omzetten naar 20XX-XX-XX notatie
        if len(datelist[0]) < 4:
            datelist[0] = "20" + datelist[0]

        if len(datelist[1]) < 2:
            datelist[1] = "0" + datelist[1]

        if len(datelist[2]) < 2:
            datelist[2] = "0" + datelist[2]

        print "-".join(datelist)
