##
# Intermediate 270 Markov Chain text generator
##

# PYTHON 2 CODE
from random import choice

# File to be read
inputfile = raw_input("Filename: ")

# Determine the size of the N-gram
N = int(raw_input("Prefix length (N): "))

# Determine the length of the generated text
n = int(raw_input("Length of generated text (n pairings): "))

# Container of key-value pairs for prefix-suffix pairs
prefixSuffixPairs = {}

# Fill prefixSuffixPairs using the text provided
with open(inputfile, "r") as f:
    # Clean up the text
    rawTextList = f.read().strip().replace("\n", " ").replace("\"", "").split(" ")
    # Make prefix and suffixes according to N
    for i in range(len(rawTextList) - N):
        prefix = " ".join([rawTextList[i + j] for j in range(N)])
        suffix = rawTextList[i + N]
        if prefix not in prefixSuffixPairs:
            prefixSuffixPairs[prefix] = [suffix]
        else:
            prefixSuffixPairs[prefix].append(suffix)

output = ""
# First iteration has no words to base the prefix on so pick a random one
prefix = choice(list(prefixSuffixPairs.keys()))
suffix = choice(prefixSuffixPairs[prefix])
output += prefix + " " + suffix
for i in range(n - 1):
    # Second iteration onwards has a string to base its prefix on
    prefix = " ".join(output.split(" ")[-N:])
    try:
        suffix = choice(prefixSuffixPairs[prefix])
    except KeyError:
        # print "NONWORD detected"
        break
    else:
        output += " " + suffix

print output

# # PYTHON 3 CODE
# from random import choice
#
# # File to be read
# inputfile = input("Filename: ")
#
# # Determine the size of the N-gram
# N = int(input("Prefix length (N): "))
#
# # Determine the length of the generated text
# n = int(input("Length of generated text (n pairings): "))
#
# # Container of key-value pairs for prefix-suffix pairs
# prefixSuffixPairs = {}
#
# # Fill prefixSuffixPairs using the text provided
# with open(inputfile, "r", encoding="latin1") as f:
#     # Clean up the text
#     rawTextList = f.read().strip().replace("\n", " ").replace("\"", "").split(" ")
#     # Make prefix and suffixes according to N
#     for i in range(len(rawTextList) - N):
#         prefix = " ".join([rawTextList[i + j] for j in range(N)])
#         suffix = rawTextList[i + N]
#         if prefix not in prefixSuffixPairs:
#             prefixSuffixPairs[prefix] = [suffix]
#         else:
#             prefixSuffixPairs[prefix].append(suffix)
#
# output = ""
# # First iteration has no words to base the prefix on so pick a random one
# prefix = choice(list(prefixSuffixPairs.keys()))
# suffix = choice(prefixSuffixPairs[prefix])
# output += prefix + " " + suffix
# for i in range(n - 1):
#     # Second iteration onwards has a string to base its prefix on
#     prefix = " ".join(output.split(" ")[-N:])
#     try:
#         suffix = choice(prefixSuffixPairs[prefix])
#     except KeyError:
#         # print "NONWORD detected"
#         break
#     else:
#         output += " " + suffix
#
# print(output.encode("unicode_escape"))
