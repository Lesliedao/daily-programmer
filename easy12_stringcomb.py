##
# String combinations
##

from itertools import permutations

rawstring = list(raw_input("> "))

for comb in permutations(rawstring, len(rawstring)):
    print ''.join(comb)
