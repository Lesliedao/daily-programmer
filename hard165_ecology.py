import random
import math

# Size of the simulation
N = int(input('Size of the forest (NxN): '))
T = int(input('Length of the simulation (ticks): '))

# Roam size of lumberjacks and bears
lumberjackRoamSize = 3
bearRoamSize = 5

# Ages when saplings turn to trees and trees turn to elders
treeAge = 12
elderAge = 120

# Sapling spawn chance of trees
treeSpawnChance = 0.1
elderSpawnChance = 0.2

# Initial percentages inhabitants
lumberjackPct = 0.1
forestPct = 0.5
bearPct = 0.02

# Keep track of all inhabitants
lumberjacks = []
trees = []
bears = []

'''
The obstacles set is only used when initiating the simulation, as inhabitants can only be placed on spots which do not contain another inhabitant.
For the remainder of the simulation, inhabitants can move freely regardless of whether the spot is occupied but different events happen depending
on which inhabitant is occupying that spot. Thus for the remainder of the simulation, multiple sets are used for the different inhabitants.
For the remainder of the simulation, the obstacles set can be used as a shorthand for all obstacles.
'''
# Keep track of all inhabitants for INITIALIZING the simulation only
obstacles = set()
# Keep track of the positions of each type of inhabitant separately for the remainder of the simulation
lumberjackPositions = set()
saplingPositions = set()
treePositions = set()
elderPositions = set()
bearPositions = set()

# Initialize a lumber, incident, tree counter
lumber = 0
lumberPrev = 0
incidents = 0
incidentsPrev = 0
newSaplings = 0
newTrees = 0
newElders = 0

def neighbours(x, y):
    n = []
    if y - 1 >= 0:
        if x - 1 >= 0: n.append((x - 1, y - 1))
        n.append((x, y - 1))
        if x + 1 < N: n.append((x + 1, y - 1))
    if x - 1 >= 0: n.append((x - 1, y))
    if x + 1 < N: n.append((x + 1, y))
    if y + 1 < N:
        if x - 1 >= 0: n.append((x - 1, y + 1))
        n.append((x, y + 1))
        if x + 1 < N: n.append((x + 1, y + 1))
    return n

class Tree(object):
    def __init__(self, t, x, y, age = 0):
        self.type = t
        self.x = x
        self.y = y
        self.age = age
        self.spawnChance = 0
        if self.type == 'T':
            self.spawnChance = treeSpawnChance
        elif self.type == 'E':
            self.spawnChance = elderSpawnChance

    def getPosition(self):
        return (self.x, self.y)

    def getAge(self):
        return self.age

    def getType(self):
        return self.type

    def spawnSapling(self):
        global newSaplings
        if random.random() < self.spawnChance:
            # Spawn a sapling
            n = list(filter(lambda x: (x not in saplingPositions) and (x not in treePositions) and (x not in elderPositions), neighbours(self.x, self.y)))
            if len(n) != 0:
                x, y = random.choice(n)
                trees.append(Tree('S', x, y))
                treePositions.add((x, y))
                newSaplings += 1

    def tick(self):
        global newTrees, newElders
        self.spawnSapling()
        self.age += 1
        if self.age == treeAge:
            saplingPositions.discard((self.x, self.y))
            treePositions.add((self.x, self.y))
            self.type = 'T'
            newTrees += 1
            self.spawnChance = treeSpawnChance
        elif self.age == elderAge:
            treePositions.discard((self.x, self.y))
            elderPositions.add((self.x, self.y))
            self.type = 'E'
            newElders += 1
            self.spawnChance = elderSpawnChance
        return self

class Lumberjack(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.alive = True

    def getPosition(self):
        return (self.x, self.y)

    def isAlive(self):
        return self.alive

    def roam(self):
        global incidents, lumber, trees
        for i in range(lumberjackRoamSize):
            n = list(filter(lambda x: x not in lumberjackPositions, neighbours(self.x, self.y)))
            if len(n) != 0:
                x, y = random.choice(n)
                lumberjackPositions.discard((self.x, self.y))
                self.x, self.y = x, y
                if (x, y) in bearPositions:
                    self.alive = False
                    incidents += 1
                    break
                elif (x, y) in treePositions:
                    lumber += 1
                    treePositions.discard((x, y))
                    trees = list(filter(lambda x: x.getPosition() != (x, y), trees))
                    break
                elif (x, y) in elderPositions:
                    lumber += 2
                    elderPositions.discard((x, y))
                    trees = list(filter(lambda x: x.getPosition() != (x, y), trees))
                    break
        if self.alive:
            lumberjackPositions.add((self.x, self.y))
        return self

class Bear(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def getPosition(self):
        return (self.x, self.y)

    def roam(self):
        global incidents, lumberjacks
        for i in range(bearRoamSize):
            n = list(filter(lambda x: x not in bearPositions, neighbours(self.x, self.y)))
            if len(n) != 0:
                x, y = random.choice(n)
                bearPositions.discard((self.x, self.y))
                self.x, self.y = x, y
                if (x, y) in lumberjackPositions:
                    # Attack the lumberjack and remove him
                    incidents += 1
                    lumberjackPositions.discard((x, y))
                    lumberjacks = list(filter(lambda x: x.getPosition() != (x, y), lumberjacks))
                    break
        bearPositions.add((self.x, self.y))
        return self


# Initialize trees
for i in range(math.trunc(forestPct * N ** 2)):
    while True and len(obstacles) < (N ** 2):
        x, y = random.randint(0, N - 1), random.randint(0, N - 1)
        if (x, y) not in obstacles:
            obstacles.add((x, y))
            treePositions.add((x, y))
            trees.append(Tree('T', x, y, treeAge))
            break

# Initialize lumberjacks
for i in range(math.trunc(lumberjackPct * N ** 2)):
    while True and len(obstacles) < (N ** 2):
        x, y = random.randint(0, N - 1), random.randint(0, N - 1)
        if (x, y) not in obstacles:
            obstacles.add((x, y))
            lumberjackPositions.add((x, y))
            lumberjacks.append(Lumberjack(x, y))
            break

# Initialize bears
for i in range(math.trunc(bearPct * N ** 2)):
    while True and len(obstacles) < (N ** 2):
        x, y = random.randint(0, N - 1), random.randint(0, N - 1)
        if (x, y) not in obstacles:
            obstacles.add((x, y))
            bearPositions.add((x, y))
            bears.append(Bear(x, y))
            break

for simulationTick in range(T):
    if simulationTick > 0 and simulationTick % 12 == 0:
        # Print yearly update
        print(f'Year [{str(simulationTick // 12).zfill(4)}]: Forest has {len(treePositions)} Trees, {len(saplingPositions)} Saplings, {len(elderPositions)} Elder Trees, {len(lumberjacks)} Lumberjacks and {len(bears)} Bears.')
        # Compare lumber count with amount of lumberjacks
        if lumber >= len(lumberjacks):
            # Add lumberjacks as a ratio of lumber cut over amount of lumberjacks
            lumberjacksHired = lumber // len(lumberjacks)
            for i in range(lumberjacksHired):
                while True and len(obstacles) < (N ** 2):
                    x, y = random.randint(0, N - 1), random.randint(0, N - 1)
                    if (x, y) not in obstacles:
                        lumberjackPositions.add((x, y))
                        lumberjacks.append(Lumberjack(x, y))
                        break
            print(f'Year [{str(simulationTick // 12).zfill(4)}]: {lumber} pieces of Lumber harvested by Lumberjacks and {lumberjacksHired} new Lumberjacks hired.')
        else:
            # Only remove a lumberjack if there is at least one
            if len(lumberjacks) > 0:
                removedLumberjack = lumberjacks.pop()
                lumberjackPositions.discard(removedLumberjack.getPosition())
                print(f'Year [{str(simulationTick // 12).zfill(4)}]: {lumber} pieces of Lumber harvested by Lumberjacks and 1 Lumberjack fired.')
        # Reset lumber count for the year and update obstacles
        lumber = 0
        lumberPrev = 0
        obstacles = saplingPositions.union(treePositions, elderPositions, lumberjackPositions, bearPositions)

        # Check the number of incidents
        if incidents > 0:
            removedBear = bears.pop()
            bearPositions.discard(removedBear.getPosition())
            print(f'Year [{str(simulationTick // 12).zfill(4)}]: 1 Bear captured by Zoo.')
        else:
            while True and len(obstacles) < (N ** 2):
                x, y = random.randint(0, N - 1), random.randint(0, N - 1)
                if (x, y) not in obstacles:
                    bearPositions.add((x, y))
                    bears.append(Bear(x, y))
                    print(f'Year [{str(simulationTick // 12).zfill(4)}]: 1 new Bear added.')
                    break
        # Reset incident count for the year and update obstacles
        incidents = 0
        incidentsPrev = 0
        obstacles = saplingPositions.union(treePositions, elderPositions, lumberjackPositions, bearPositions)

    newSaplings = 0
    newTrees = 0
    newElders = 0

    # Advance the trees one tick
    trees = list(map(lambda x: x.tick(), trees))

    # Let the lumberjacks roam
    lumberjacks = list(map(lambda x: x.roam(), lumberjacks))
    # Remove dead lumberjacks
    lumberjacks = list(filter(lambda x: x.isAlive(), lumberjacks))

    # Let the bears roam
    bears = list(map(lambda x: x.roam(), bears))

    # Ensure the number of lumberjacks does not fall below 1
    if len(lumberjacks) == 0:
        while True and len(obstacles) < (N ** 2):
            x, y = random.randint(0, N - 1), random.randint(0, N - 1)
            if (x, y) not in obstacles:
                lumberjackPositions.add((x, y))
                lumberjacks.append(Lumberjack(x, y))
                break

    # Make a shorthand for all obstacles
    obstacles = saplingPositions.union(treePositions, elderPositions, lumberjackPositions, bearPositions)
    print(f'Month {str(simulationTick + 1).zfill(4)}: [{len(obstacles)}] spots occupied')
    print(f'Month {str(simulationTick + 1).zfill(4)}: [{lumber - lumberPrev}] pieces of lumber harvested by Lumberjacks')
    print(f'Month {str(simulationTick + 1).zfill(4)}: [{newSaplings}] new Saplings created')
    print(f'Month {str(simulationTick + 1).zfill(4)}: [{incidents - incidentsPrev}] Lumberjacks were Mauled by Bears')
    print(f'Month {str(simulationTick + 1).zfill(4)}: [{newTrees}] Saplings turned into Trees')
    print(f'Month {str(simulationTick + 1).zfill(4)}: [{newElders}] Trees turned into Elder Trees')
    lumberPrev = lumber
    incidentsPrev = incidents

    if len(trees) == 0:
        print('No more trees left')
        break
