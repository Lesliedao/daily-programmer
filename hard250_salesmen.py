import random
import math
import time
import matplotlib.pyplot as plt

class GeneticSalesmen(object):
    def __init__(self, inputf, size, timeBound):
        self.ALL_CITIES = {}
        self.MOTHER_PROB = 0.5
        self.MUTATION_PROB = 0.85
        self.SURVIVAL_FRAC = 0.25
        self.size = size
        self.timeBound = timeBound
        with open(inputf, 'r') as f:
            for count, line in enumerate(f):
                self.ALL_CITIES[count] = dict((x, y) for x, y in list(zip(['x', 'y'], list(map(lambda n: int(n), line.strip().split(' '))))))
        self.pop = [self.randomRoute(len(self.ALL_CITIES.keys()) - 1) for _ in range(self.size)]
        self.history = []

    def fitness(self, route):
        s = 0
        for a, b in list(zip(route[:-1], route[1:])):
            s += math.trunc(math.sqrt((self.ALL_CITIES[a]['x'] - self.ALL_CITIES[b]['x']) ** 2 + (self.ALL_CITIES[a]['y'] - self.ALL_CITIES[b]['y']) ** 2))
        return s

    def crossover(self, mother, father):
        r = [0]
        for i in range(len(mother) - 2):
            if (random.random() < self.MOTHER_PROB and mother[i + 1] not in r) or father[i + 1] in r:
                r.append(mother[i + 1])
            else:
                r.append(father[i + 1])
        r.append(0)
        # Check if all cities appear in the crossover
        for i in range(1, len(mother) - 1):
            if r.count(i) < 1:
                for j in range(1, len(mother) - 1):
                    if r.count(j) > 1:
                        r[r.index(j)] = i
                        break
        if random.random() < self.MUTATION_PROB:
            a, b = random.sample(list(range(1, len(mother) - 1)), 2)
            r[a], r[b] = r[b], r[a]
        return r

    def advanceGeneration(self):
        self.history.append(min(self.pop, key = self.fitness))
        newPop = sorted(self.pop, key = self.fitness)[:math.trunc(len(self.pop) * self.SURVIVAL_FRAC)]
        self.pop = newPop
        while len(newPop) < self.size:
            newPop.append(self.crossover(*random.sample(self.pop, 2)))
        self.pop = newPop

    def randomRoute(self, n):
        return [0, *sorted(list(range(1, n + 1)), key = lambda _: random.random()), 0]

    def pprintRoute(self, r):
        print('Route: ', end = '')
        for i in range(len(r) - 1):
            print(f'{r[i]} -> ', end = '')
        print(f'{r[-1]}')

    def routeHistory(self):
        for i, r in enumerate(self.history):
            print(f'Gen {i}: {" -> ".join(map(lambda x: str(x), r))}')

    def fitnessHistory(self):
        for i, r in enumerate(self.history):
            print(f'Gen {i}: {self.fitness(r)}')

    def random(self):
        r = self.randomRoute(len(self.ALL_CITIES.keys()) - 1)
        print(f'Random route is {self.fitness(r)} in length')
        self.pprintRoute(r)
        x = [self.ALL_CITIES[c]['x'] for c in r]
        y = [self.ALL_CITIES[c]['y'] for c in r]
        plt.plot(x, y)
        plt.plot(x, y, 'ro')
        plt.show()

    def run(self):
        t = time.time()
        gen = 0
        while time.time() - t < self.timeBound:
            gen += 1
            self.advanceGeneration()
        bestRoute = min(self.pop, key = self.fitness)
        print(f'Elapsed time: {(time.time() - t):.5} seconds')
        print(f'Found a route in {gen} generations of {self.fitness(bestRoute)} in length')
        self.pprintRoute(bestRoute)
        x = [self.ALL_CITIES[r]['x'] for r in bestRoute]
        y = [self.ALL_CITIES[r]['y'] for r in bestRoute]
        plt.plot(x, y)
        plt.plot(x, y, 'ro')
        plt.show()

s = GeneticSalesmen('hard250_salesmen_input40.txt', 100, 1)
s.random()
s.run()
s.fitnessHistory()
