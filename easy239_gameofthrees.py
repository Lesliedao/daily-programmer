## Game of Threes
## Neemt een positieve integer en
## - deelt door drie als dat kan (print "0"); of
## - +1 of -1 zodat het getal wel deelbaar is door 3 (print "-1" of "+1")
## De laatste output is altijd 1.

print "Game of Threes"
while True:
	rawnumber = raw_input("Positive integer: ")
	try:
		number = int(rawnumber)
	except ValueError:
		continue
	else:
		if number < 1:
			continue
		else:
			break
			
while number != 1:
	if number % 3 == 0:
		print number, "0"
	elif (number + 1) % 3 == 0:
		print number, "+1"
		number += 1
	else:
		print number, "-1"
		number -= 1
	number /= 3

print number