##
# Guess that number game
##

from random import randint

thenumber = randint(1, 100)

print "Welcome to Guess that Number! I have picked a number in [1, 100]. Please make a guess. Type 'exit' to quit."
while True:
	rawinp = raw_input("")
	if rawinp == "exit":
		print "Program terminated. The number was", thenumber
		break
	else:
		try:
			inp = int(rawinp)
		except ValueError:
			print "Please enter an integer in [1, 100] or 'exit' to quit."
			continue
		else:
			if inp < 1 or inp > 100:
				print "Please enter an integer in [1, 100] or 'exit' to quit."
				continue
			elif inp == thenumber:
				print "Correct! That is my number, you win! Terminating program."
				break
			elif inp < thenumber:
				print "Wrong. That number is below my number."
				continue
			else:
				print "Wrong. That number is above my number."
				continue