##
# Unbounded knapsack
##

from collections import defaultdict

goal = 500
market = []

# Pak de market uit een externe file
with open("int243_knapsack_input.txt", "r") as f:
    for line in f:
        fruit, price = line.split()
        market.append((fruit, int(price)))

def decide(fruits, goal):
    # Als de goal 0 is, kun je niks meer toevoegen
    if goal == 0:
        # Defaultdict maakt een key aan met initiele waarde 0 als de key niet bestaat
        return [defaultdict(int)]

    # Als de goal negatief is, of er geen fruits meer zijn, zijn er geen oplossingen
    if goal < 0 or not fruits:
        return []

    # Grijp het fruit en de prijs uit de fruits list
    fruit, price = fruits[0]

    # Bepaal de oplossingen als je 1 van het fruit koopt (en recursief of je meer koopt)
    add = decide(fruits, goal - price)
    # Voor elk antwoord dat gevonden is, is de hoeveelheid fruit 1 te laag vanwege recursie
    for answer in add:
        answer[fruit] += 1

    # Bepaal de oplossingen als je het fruit niet koopt
    leave = decide(fruits[1:], goal)

    # De oplossingen zijn de beide lists geconcatenated
    return add + leave

answers = decide(market, goal)
# Print elk antwoord
for answer in answers:
    line = []
    # Grijp de "key-value pairs" (tuple) uit de list
    for k, v in answer.items():
        # Check op meervoud
        if v > 1:
            k = k + "s"

        line.append(str(v) + " " + k)

    print ", ".join(line)
