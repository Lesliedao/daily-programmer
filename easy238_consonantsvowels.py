##
# Consonants and vowels
##

from random import choice

consonants = list("bcdfghjklmnpqrstvwxyz")
vowels = list("aeiou")

prompt = raw_input("> ")

output = []
for char in prompt:
    if char.isupper():
        if char == "C":
            output.append(choice(consonants).upper())
        elif char == "V":
            output.append(choice(vowels).upper())
        else:
            pass
    else:
        if char == "c":
            output.append(choice(consonants))
        elif char == "v":
            output.append(choice(vowels))
        else:
            pass

print "".join(output)
