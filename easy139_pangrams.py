##
# Pangrams
##

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

while True:
    rawn = raw_input("Number of strings: ")
        try:
            n = int(rawn)
        except ValueError:
            continue
        else:
            if n < 1:
                continue
            else:
                break

strings = []
results = []
for i in range(n):
    strings.append(raw_input("> ").lower())
        letters = 0
        for letter in alphabet:
            if letter in strings[i]:
                letters += 1
            else:
                pass
        if letters == 26:
            results.append(True)
        else:
            results.append(False)
        print results[i]
