from math import sqrt

class Complex(object):
    def __init__(self, r, i):
        assert isinstance(r, (int, float)) and isinstance(i, (int, float)), 'Real and imaginary parts must be floating point numbers or integers'
        self.r = r
        self.i = i

    def getModulus(self):
        return sqrt(self.r**2 + self.i**2)

    def getConjugate(self):
        return Complex(self.r, -self.i)

    def add(self, other):
        assert isinstance(other, Complex), 'Argument must be an instance of Complex'
        return Complex(self.r + other.r, self.i + other.i)

    def subtract(self, other):
        assert isinstance(other, Complex), 'Argument must be an instance of Complex'
        return Complex(self.r - other.r, self.i - other.i)

    def multiply(self, other):
        assert isinstance(other, Complex), 'Argument must be an instance of Complex'
        return Complex(self.r * other.r - self.i * other.i, self.r * other.i + self.i * other.r)

    def toString(self):
        if self.i < 0:
            return '{0:.2f}{1:.2f}i'.format(self.r, self.i)
        else:
            return '{0:.2f}+{1:.2f}i'.format(self.r, self.i)

c = Complex(2, -3)
z = Complex(5, 1)

