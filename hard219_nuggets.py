# pylint: disable=C0103,C0111,C0303,C0301
##
# The cave of prosperity (0/1 Knapsack)
##

with open("hard219_nuggets_input3.txt", "r") as f:
    linecount = 0
    for line in f:
        if linecount == 0:
            capacity = int(line.strip().replace(".", ""))
            linecount += 1
        elif linecount == 1:
            nNuggets = int(line.strip())
            break
    nuggets = []
    for line in f:
        nuggets.append(int(line[2:].strip()))

def decide(nugs, cap):
    if cap <= 0 or not nugs:
        return [[]]

    take = decide(nugs[1:], cap - nugs[0])
    for answer in take:
        answer.append(nugs[0])

    leave = decide(nugs[1:], cap)

    return take + leave

answers = [answer for answer in decide(nuggets, capacity) if sum(answer) <= capacity]
best = max(answers, key=lambda x: sum(x))
print sum(best) / float(10000000)
for nug in best:
    print nug / float(10000000)
