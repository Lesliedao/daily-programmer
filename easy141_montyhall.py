## Monty Hall simulation
## Neemt een positieve integer als aantal keer dat het Monty Hall
## probleem gesimuleerd moet worden voor beide tactieken: van deur switchen en
## niet van deur switchen. Print voor beide tactieken het percentage successen.

from random import choice

while True:
	rawN = raw_input("Amount of simulations: ")
	try:
		N = int(rawN)
	except ValueError:
		continue
	else:
		if N < 1:
			continue
		else:
			break
			
doors = [1, 2, 3]
successswitch = 0.0
successstay = 0.0

for i in range(0, N):
	car = choice(doors)
	pick = choice(doors)
	if pick == car:
		successstay += 1
	else:
		successswitch += 1

print "Success rate staying: %.2f%%" % (successstay * 100 / N)
print "Success rate switching: %.2f%%" % (successswitch * 100 / N)