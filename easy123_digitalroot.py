##
# Digital root
##

while True:
	rawinp = raw_input("Non-negative integer: ")
	try:
		inp = int(rawinp)
	except ValueError:
		continue
	else:
		if inp < 0:
			continue
		else:
			break
	
while True:
	sum = 0
	for digit in map(int, str(inp)):
		sum += digit
	if sum < 10:
		break
	else:
		inp = sum
		continue

print sum