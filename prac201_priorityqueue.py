##
# Priority queue met twee priority waarden
##

import os.path
import pickle

class PriorityQueue(object):
    def __init__(self, items, nameA, nameB):
        self.queue = []
        self.string = items
        self.prA = nameA
        self.prB = nameB

    def enqueue(self, item, A, B):
        self.queue.append({self.string: item, self.prA: A, self.prB: B})

    def dequeueA(self):
        if len(self.queue) != 0:
            self.queue.remove(max(self.queue, key = lambda x: x[self.prA]))

    def dequeueB(self):
        if len(self.queue) != 0:
            self.queue.remove(max(self.queue, key = lambda x: x[self.prB]))

    def count(self):
        return len(self.queue)

    def clear(self):
        self.queue = []

    def dequeueFirst(self):
        if len(self.queue) != 0:
            self.queue.pop(0)

    def load(self, infile):
        if (os.path.isfile(infile)):
            with open(infile, "rb") as f:
                obj = pickle.load(f)
                if type(obj) is list:
                    self.queue.extend(obj)
                else:
                    print "Invalid variable"
        else:
            print "File", infile, "does not exist"

    def save(self, outfile):
        with open(outfile, "wb") as f:
            pickle.dump(self.queue, f)

    def view(self):
        print self.queue


q = PriorityQueue("Item", "Priority", "Cost")
q.enqueue("Doritos", 7, 1.99)
q.enqueue("Mtn dew", 10, 1.50)
q.view()
q.save("queue.txt")
