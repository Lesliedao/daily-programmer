##
# Probability distribution of a 6-sided die
##

from random import randint

res1 = [0, 0, 0, 0, 0, 0]
res2 = [0, 0, 0, 0, 0, 0]
res3 = [0, 0, 0, 0, 0, 0]
res4 = [0, 0, 0, 0, 0, 0]
res5 = [0, 0, 0, 0, 0, 0]
res6 = [0, 0, 0, 0, 0, 0]

for i in range(10):
	res1[randint(1, 6) - 1] += 1

for i in range(100):
	res2[randint(1, 6) - 1] += 1

for i in range(1000):
	res3[randint(1, 6) - 1] += 1

for i in range(10000):
	res4[randint(1, 6) - 1] += 1

for i in range(100000):
	res5[randint(1, 6) - 1] += 1
	
for i in range(1000000):
	res6[randint(1, 6) - 1] += 1
	
for i in range(6):
	res1[i] /= round(10.0, 2)
	res2[i] /= round(100.0, 2)
	res3[i] /= round(1000.0, 2)
	res4[i] /= round(10000.0, 2)
	res5[i] /= round(100000.0, 2)
	res6[i] /= round(1000000.0, 2)

print "# of Rolls\t1s\t2s\t3s\t4s\t5s\t6s"
print "="*60
print "10\t\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f" % (res1[0], res1[1], res1[2], res1[3], res1[4], res1[5])
print "100\t\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f" % (res2[0], res2[1], res2[2], res2[3], res2[4], res2[5])
print "1000\t\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f" % (res3[0], res3[1], res3[2], res3[3], res3[4], res3[5])
print "10000\t\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f" % (res4[0], res4[1], res4[2], res4[3], res4[4], res4[5])
print "100000\t\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f" % (res5[0], res5[1], res5[2], res5[3], res5[4], res5[5])
print "1000000\t\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f" % (res6[0], res6[1], res6[2], res6[3], res6[4], res6[5])